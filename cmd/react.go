/*
Copyright © 2022 FleetOps

*/
package cmd

import (
	"github.com/spf13/cobra"
)

// reactCmd represents the react command
var reactCmd = &cobra.Command{
	Use:   "react",
	Short: "Commands to help us in our day to day react development",
	Long:  `We can create components and export them to a centralized components repo, for now.`,
	// Run: func(cmd *cobra.Command, args []string) {
	// 	fmt.Println("react called")
	// },
}

func init() {
	rootCmd.AddCommand(reactCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// reactCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// reactCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
