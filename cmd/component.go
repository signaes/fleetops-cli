/*
Copyright © 2022 FleetOps

*/
package cmd

import (
	"bytes"
	"embed"
	"fmt"
	"log"
	"os"
	"path"
	"strings"
	"text/template"

	"github.com/spf13/cobra"
)

type Component struct {
	Name string
}

var (
	//go:embed templates/react/create/component/*
	templatesFS embed.FS
)

// componentCmd represents the component command
var componentCmd = &cobra.Command{
	Use:   "component",
	Short: "Create a new component package",
	Long: `You can use this command to create a new React component it will create boilerplate files for:

- the component file itself
- a test file
- a storybook file

Under a directory with the component name.`,
	Run: func(cmd *cobra.Command, args []string) {
		name, err := cmd.Flags().GetString("name")

		if name == "" || err != nil {
			log.Fatalln("We need the component name. You can provide it by passing the --name flag with some value")
		}

		dir, err := cmd.Flags().GetString("path")

		if err != nil {
			log.Fatalln(err)
		}

		formatedName := FormatComponentName(name)

		fmt.Println("Creating", formatedName, "in", dir)

		makeDirs(path.Join(dir, formatedName))

		files := map[string]string{
			"component.index":   path.Join(dir, formatedName, "index.ts"),
			"component.styled":  path.Join(dir, formatedName, "styled.tsx"),
			"component.main":    path.Join(dir, formatedName, formatedName+".tsx"),
			"component.test":    path.Join(dir, formatedName, formatedName+".test.tsx"),
		}

		createStoryFile, err := cmd.Flags().GetBool("stories")

    if err != nil {
			log.Fatalln(err)
		}

    if createStoryFile {
      files["component.stories"] = path.Join(dir, formatedName, formatedName+".stories.tsx")
    }

		for name, path := range files {
			writeFile(path, getExecutedTemplateBytes(name, formatedName))
		}
	},
}

func init() {
	createCmd.AddCommand(componentCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// componentCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// componentCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")

	componentCmd.Flags().StringP("name", "n", "", "The component name")
	componentCmd.Flags().StringP("path", "p", ".", "Where to create the new component package in")
	componentCmd.Flags().BoolP("stories", "s", false, "Whether to create a storybook file or not")
}

func FormatComponentName(s string) string {
	return DashedToCamelCase(s)
}

func Cap(s string) string {
	return strings.ToUpper(s[:1]) + s[1:]
}

func DashedToCamelCase(s string) string {
	result := ""

	for _, item := range strings.Split(s, "-") {
		result += Cap(item)
	}

	return result
}

func makeDirs(path string) {
	if fileInfo, err := os.Stat(path); os.IsNotExist(err) {
		err = os.MkdirAll(path, os.ModePerm)

		if err != nil {
			log.Fatalln(err)
		}
	} else {
		if !fileInfo.IsDir() {
			err = os.MkdirAll(path, os.ModePerm)

			if err != nil {
				log.Fatalln(err)
			}
		}
	}
}

func writeFile(path string, b []byte) {
	if err := os.WriteFile(path, b, os.ModePerm); err != nil {
		log.Fatalln(err)
	}
}

func getExecutedTemplateBytes(name string, componentName string) []byte {
	tpl, err := template.ParseFS(templatesFS, "templates/react/create/component/*")

	if err != nil {
		log.Fatalln(err)
	}

	buffer := &bytes.Buffer{}

	err = tpl.ExecuteTemplate(buffer, name, Component{componentName})

	if err != nil {
		log.Fatalln(err)
	}

	return buffer.Bytes()
}
