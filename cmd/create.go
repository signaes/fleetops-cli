/*
Copyright © 2022 FleetOps

*/
package cmd

import (
	"github.com/spf13/cobra"
)

// createCmd represents the create command
var createCmd = &cobra.Command{
	Use:   "create",
	Short: "Boilerplates creator",
	Long:  `We can use it to create new components packages, for example.`,
	// Run: func(cmd *cobra.Command, args []string) {
	// 	fmt.Println("create called")
	// },
}

func init() {
	reactCmd.AddCommand(createCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// createCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// createCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
