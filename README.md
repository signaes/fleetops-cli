# fo

## FleetOps CLI

### How to set this up?

The easiest way to get this on your machine at the moment is probably by cloning this repo.

Then you can do the following (make sure you have [golang](https://go.dev/) installed locally):

#### Build

```
go build -o fo ./main.go
```

#### Make it available elsewhere by moving the binary to some directory in your $PATH*

* In the example below `usr/local/bin`

```
mv fo /usr/local/bin/
```

#### Use it

The only commands available so far are for creating a react component.
This can be done like in the following example (the name of the component can be separated by a `-` and will be turned into a camel cased name)

```
fo react create component -n new-slider-component -p . -s
```

You can pass in the component name with the `-n` flag and the place where to create the component in with the `-p` flag.
The `-s` flag is optional and will result in the creation of a boilerplatte storybook file ending in `stories.tsx` if used.
The command above would create the following files:

```
NewSliderComponent
├── NewSliderComponent.stories.tsx
├── NewSliderComponent.test.tsx
├── NewSliderComponent.tsx
├── index.ts
└── styled.tsx
```

The command below, without the `-s` flag:

```
fo react create component -n new-slider-component -p .
```

Would result in this output:

```
NewSliderComponent
├── NewSliderComponent.test.tsx
├── NewSliderComponent.tsx
├── index.ts
└── styled.tsx
```


## Command line autocompletion

This was built with cobra with has a couple of cool features including the
ability to generate autocompletion for your terminal, based on the available
commands, so if you run `fo completion` you should see something like this:

```
Generate the autocompletion script for fo for the specified shell.
See each sub-command's help for details on how to use the generated script.

Usage:
  fo completion [command]

Available Commands:
  bash        Generate the autocompletion script for bash
  fish        Generate the autocompletion script for fish
  powershell  Generate the autocompletion script for powershell
  zsh         Generate the autocompletion script for zsh

Flags:
  -h, --help   help for completion

Use "fo completion [command] --help" for more information about a command.
```

You can then generate and use the autocompletion function like this (using zsh
as an example):

```
fo completion zsh | pbcopy
pbpaste > _fo
```
Then you will need to make it executable and put it somewhere in your `$PATH` like:

```
echo $PATH
chmod u+x _fo
mv _fo /usr/local/share/zsh/site-functions/
```
