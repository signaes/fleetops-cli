/*
Copyright © 2022 FleetOps

*/
package main

import "github.com/FleetOps/cli/cmd"

func main() {
	cmd.Execute()
}
